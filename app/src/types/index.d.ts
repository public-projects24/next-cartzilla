declare global {
    type Product = {
        id: number;
        title: string;
        price: number;
        description: string;
        category: object;
        image: string;
        //images: string[];
        //creationAt: string;
        //updatedAt: string;
    };
    type CartProduct = {
        id: number;
        title: string;
        price: number;
        description: string;
        category: object;
        image: string;
        quantity: number;
    };
}

export {};
