import "@/assets/css/components/home/trending-section.css";
import TitleSection from "@/components/common/TitleSection";
import ProductCard from "@/components/common/ProductCard";
import { useEffect, useState } from "react";
import productService from "@/services/product";
//import { Link } from "react-router-dom";
//import { MdChevronRight } from "react-icons/md";

const getProducts = async () => {
    return await productService.limit(20);
};

const ProductList = async () => {
    /* const [products, setProducts] = useState<Product[]>([]);

    // On mounted
    useEffect(() => {
        // Get products
        async function getProducts() {
            const response = await productService.limit(20);
            //console.log("response ", response.data);
            setProducts(response.data);
        }
        getProducts();
    }, []); */

    const response = await getProducts();
    const products: Product[] = response.data;

    const productsList = products.map((product) => (
        <ProductCard key={product.id} product={product} />
    ));

    return (
        <div className="trending-section container mx-auto">
            <TitleSection title="Our Products" />
            <div className="grid sm:grid-cols-2 lg:grid-cols-4 gap-1">
                {productsList}
            </div>
            {/* <div className="trending-section__more-products">
                <Link to="products" className="btn btn--outline">
                    More products
                    <MdChevronRight className="btn__icon" />
                </Link>
            </div> */}
        </div>
    );
};

export default ProductList;
