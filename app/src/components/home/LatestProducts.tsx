import product from "@/services/product";
import TrendingSection from "@/components/home/TrendingSection";

const getLatestProducts = async () => {
    return await product.limit(8);
};
export default async function LatestProducts() {
    const latestProducts = await getLatestProducts();

    return (
        <TrendingSection
            title="Latest products"
            products={latestProducts.data}
        />
    );
}
