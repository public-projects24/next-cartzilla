import product from "@/services/product";
import TrendingSection from "@/components/home/TrendingSection";

const getLatestProducts = async () => {
    return await product.limit(5);
};
export default async function TrendingProducts() {
    const latestProducts = await getLatestProducts();

    return (
        <TrendingSection
            title="Trending products"
            products={latestProducts.data}
        />
    );
}
