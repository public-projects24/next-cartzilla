import "@/assets/css/components/home/trending-section.css";
import TitleSection from "@/components/common/TitleSection";
import ProductCard from "@/components/common/ProductCard";
import Link from "next/link";
import { MdChevronRight } from "react-icons/md";
type TrendingSectionProps = {
    title: string;
    products: Product[];
};

const TrendingSection = (props: TrendingSectionProps) => {
    const productsList = props.products.map((product) => (
        <ProductCard key={product.id} product={product} />
    ));

    return (
        <section className="trending-section container mx-auto">
            <TitleSection title={props.title} />
            <div className="grid sm:grid-cols-2 lg:grid-cols-4 gap-1">
                {productsList}
            </div>
            <div className="trending-section__more-products">
                <Link href="products" className="btn btn--outline">
                    More products
                    <MdChevronRight className="btn__icon" />
                </Link>
            </div>
        </section>
    );
};

export default TrendingSection;
