import "@/assets/css/components/common/banner.css";
import { MdChevronRight } from "react-icons/md";

const Banner = () => {
    return (
        <div className="banner">
            <div className="container mx-auto banner__container">
                <div className="banner__info">
                    <h2 className="banner__subtitle">Has just arrived!</h2>
                    <h1 className="banner__title">Huge Summer Collection</h1>
                    <p className="banner__excerpt">
                        Swimwear, Tops, Shorts, Sunglasses & much more...
                    </p>
                    <a href="" className="btn btn--primary">
                        Shop Now
                        <MdChevronRight className="btn__icon" />
                    </a>
                </div>
                <div className="banner__image">
                    <img src="/img/home/banner-blue.jpg" alt="banner" />
                </div>
            </div>
        </div>
    );
};

export default Banner;
