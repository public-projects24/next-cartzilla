import "@/assets/css/components/common/title-section.css";

type TitleSectionProps = {
    title: string;
};
const TitleSection = (props: TitleSectionProps) => {
    return (
        <>
            <div className="title-section">
                <h2 className="title-section__title">{props.title}</h2>
            </div>
        </>
    );
};

export default TitleSection;
