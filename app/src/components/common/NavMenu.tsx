"use client";
import "@/assets/css/components/common/navmenu.css";
import Link from "next/link";
import { usePathname, useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "@/hooks/reduxHooks";
import { logoutRequest } from "@/store/authSlice";
import { MdMenu, MdOutlineShoppingBag } from "react-icons/md";

type PropsType = {
    jwtCookieExistAction: any;
};

export default function NavMenu(props: PropsType) {
    const dispatch = useAppDispatch();
    const auth = useAppSelector((state) => state.authState);
    const cart = useAppSelector((state) => state.cartState);
    const [showMobileMenu, setShowMobileMenu] = useState(false);
    const pathname = usePathname();
    const [isMounted, setIsMounted] = useState(false);
    const router = useRouter();

    const handleLogout = async () => {
        await dispatch(logoutRequest());
        router.refresh();
    };

    useEffect(() => {
        setIsMounted(true);

        const checkJwt = async () => {
            const jwtExist = await props.jwtCookieExistAction();

            if (auth.isAuthenticated && !jwtExist) {
                console.log("se debe cerrar sesión");
                handleLogout();
            }
        };

        checkJwt();
    }, []);

    const handleToggleMobileMenu = () => {
        console.log("show mobile menu");
        setShowMobileMenu(!showMobileMenu);
    };

    const handleCloseMenu = () => {
        console.log("handle close menu");
        setShowMobileMenu(false);
    };

    return (
        <nav className="navmenu">
            <div className="navmenu__container container mx-auto">
                <div className="navmenu__logo">
                    <Link onClick={handleCloseMenu} href="/">
                        <img
                            src="/img/common/logo-mobile.png"
                            alt="logo"
                            className="navmenu__logo-img lg:hidden"
                        />
                    </Link>
                    <Link onClick={handleCloseMenu} href="/">
                        <img
                            src="/img/common/logo-dark.png"
                            alt="logo"
                            className="navmenu__logo-img hidden lg:block"
                        />
                    </Link>
                </div>
                <div
                    className={`navmenu__collapsable ${
                        showMobileMenu ? "navmenu__collapsable--active" : ""
                    }`}
                >
                    <ul className="navmenu__list">
                        <li className="navmenu__list-item">
                            <Link
                                onClick={handleCloseMenu}
                                href="/"
                                className={
                                    pathname === "/"
                                        ? `navmenu__list-link navmenu__list-link--active`
                                        : `navmenu__list-link`
                                }
                            >
                                Home
                            </Link>
                        </li>
                        <li className="navmenu__list-item">
                            <Link
                                onClick={handleCloseMenu}
                                href="/products"
                                className={
                                    pathname === "/products"
                                        ? `navmenu__list-link navmenu__list-link--active`
                                        : `navmenu__list-link`
                                }
                            >
                                Products
                            </Link>
                        </li>
                        <li className="navmenu__list-item">
                            <Link
                                onClick={handleCloseMenu}
                                href="/profile"
                                className={
                                    pathname === "/profile"
                                        ? `navmenu__list-link navmenu__list-link--active`
                                        : `navmenu__list-link`
                                }
                            >
                                Profile
                            </Link>
                        </li>
                        <li className="navmenu__list-item">
                            <Link
                                onClick={handleCloseMenu}
                                href="/admin"
                                className={
                                    pathname === "/admin"
                                        ? `navmenu__list-link navmenu__list-link--active`
                                        : `navmenu__list-link`
                                }
                            >
                                Admin
                            </Link>
                        </li>
                        {/* <li className="navmenu__list-item">
                            <Link
                                onClick={handleCloseMenu}
                                href="/admin/v2"
                                className={({ isActive }) =>
                                    isActive
                                        ? `navmenu__list-link navmenu__list-link--active`
                                        : `navmenu__list-link`
                                }
                            >
                                Admin V2
                            </Link>
                        </li> */}
                        <li className="navmenu__list-item lg:hidden">
                            {isMounted && auth.isAuthenticated && (
                                <>
                                    <button
                                        className="btn btn--outline lg:hidden"
                                        onClick={() => {
                                            handleLogout();
                                        }}
                                    >
                                        Log out
                                    </button>
                                </>
                            )}
                            {isMounted && !auth.isAuthenticated && (
                                <Link
                                    onClick={handleCloseMenu}
                                    href="login"
                                    className="btn btn--primary lg:hidden"
                                >
                                    Login
                                </Link>
                            )}
                        </li>
                    </ul>
                </div>
                <div className="navmenu__buttons">
                    <Link
                        onClick={handleCloseMenu}
                        href="/cart"
                        className="navmenu__cart-button"
                    >
                        <span className="navmenu__cart-counter">
                            {isMounted && cart.products.length}
                        </span>
                        <i className="navmenu__cart-icon">
                            <MdOutlineShoppingBag />
                        </i>
                    </Link>

                    <button
                        className="navmenu__cart-hamburger lg:hidden"
                        onClick={handleToggleMobileMenu}
                    >
                        <MdMenu />
                    </button>

                    {isMounted && auth.isAuthenticated && (
                        <>
                            <button
                                className="btn btn--outline hidden lg:block"
                                onClick={() => {
                                    handleLogout();
                                }}
                            >
                                Log out
                            </button>
                        </>
                    )}
                    {isMounted && !auth.isAuthenticated && (
                        <Link
                            onClick={handleCloseMenu}
                            href="login"
                            className="btn btn--primary hidden lg:block"
                        >
                            Login
                        </Link>
                    )}
                </div>
            </div>
        </nav>
    );
}
