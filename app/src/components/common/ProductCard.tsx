"use client";
import "@/assets/css/components/common/product-card.css";
import { useAppDispatch } from "@/hooks/reduxHooks";
import { add } from "@/store/cartSlice";

type ProductCardProps = {
    product: Product;
};

const ProductCard = ({ product }: ProductCardProps) => {
    const dispatch = useAppDispatch();

    const onAddToCart = () => {
        dispatch(add(product));
    };

    return (
        <div className="product-card">
            {/* <a href="#" className="product-card__link"> */}
            <img
                src={product.image}
                alt="product"
                className="product-card__image"
            />
            <h3 className="product-card__title">
                {product.title.substring(0, 36)}
            </h3>
            <p className="product-card__price">${product.price}</p>
            <button
                className="btn btn--primary product-card__btn"
                onClick={onAddToCart}
            >
                Add to cart
            </button>
            {/* </a> */}
        </div>
    );
};

export default ProductCard;
