"use client";
import { useEffect, useState } from "react";
import product from "@/services/product";
import profileService from "@/services/profile";
import useAxiosPrivate from "@/hooks/useAxiosPrivate";

type User = {
    name: string;
    email: string;
};

export default function Profile() {
    const [user, setUser] = useState<User>();
    const axiosPrivate = useAxiosPrivate();

    useEffect(() => {
        let isMounted = true;
        const controller = new AbortController();

        const getUser = async () => {
            try {
                //const response = await userService.get();
                /* const response = await userService.get("/user", {
                    signal: controller.signal,
                }); */
                const response = await axiosPrivate.get("/profile", {
                    signal: controller.signal,
                });
                console.log("response ", response.data);
                isMounted && setUser(response.data.data);

                //setLoading(false);
                //return response.data;
            } catch (error) {
                console.log("el error es ", error);
                //return error;
            }
        };
        getUser();

        return () => {
            isMounted = false;
            controller.abort();
        };

        /* const getProfile = async () => {
            const response = await profileService.get();
            setUser(response.data.data);
        };

        getProfile(); */
    }, []);

    return (
        <div>
            {!user && <div>Loading profile ...</div>}
            {user && (
                <form>
                    <input
                        type="text"
                        value={user?.name}
                        onChange={(e) =>
                            setUser({ ...user, name: e.target.value })
                        }
                    />
                    <input
                        type="text"
                        value={user?.email}
                        onChange={(e) =>
                            setUser({ ...user, email: e.target.value })
                        }
                    />
                </form>
            )}
        </div>
    );
}
