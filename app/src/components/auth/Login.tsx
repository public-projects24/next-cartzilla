"use client";
import "@/assets/css/components/auth/auth.css";
import TitleSection from "../common/TitleSection";
import React, { useEffect, useState } from "react";

import { useAppDispatch, useAppSelector } from "@/hooks/reduxHooks";
import {
    loginRequest,
    clearErrorMessage,
    logoutRequest,
} from "@/store/authSlice";
import { useRouter, useSearchParams } from "next/navigation";

type PropsType = {
    jwtCookieExistAction: any;
    revalidateUrlAction: any;
};

export default function Login(props: PropsType) {
    const dispatch = useAppDispatch();
    const auth = useAppSelector((state) => state.authState);

    const [credentials, setCredentials] = useState({
        email: "admin@gmail.com",
        password: "123456",
    });

    const router = useRouter();
    const searchParams = useSearchParams();

    useEffect(() => {
        const checkJwt = async () => {
            const jwtExist = await props.jwtCookieExistAction();

            if (auth.isAuthenticated && !jwtExist) {
                console.log(" se debe cerrar sesión");
                dispatch(logoutRequest());
            }
        };

        checkJwt();
    }, []);

    const onSubmitForm = async (e: React.SyntheticEvent) => {
        e.preventDefault();

        // Form 1
        dispatch(clearErrorMessage());
        await dispatch(loginRequest(credentials));

        const requested = searchParams.get("requested") || "/";
        console.log("Redireccionar a requested ", requested);
        props.revalidateUrlAction(requested);

        /* router.push(requested);
        console.log("ya se redirecciono"); */

        // Form 2
        //setErrorMessage("");
        /* try {
            const response = await auth.login(credentials);
            console.log("response ", response);

            dispatch(
                login({
                    token: response.data.token,
                    username: credentials.username,
                })
            );
            router.replace("/");
        } catch (error: any) {
            if (error.response) {
                setErrorMessage(error.response.data);
            }
        } */
    };

    return (
        <div className="auth container mx-auto">
            <TitleSection title="Sign in" />
            <form action="" onSubmit={onSubmitForm} noValidate>
                <div className="form-group">
                    <label className="label" htmlFor="email">
                        Email address
                    </label>
                    <input
                        className="input"
                        id="email"
                        type="email"
                        placeholder="johndoe@example.com"
                        value={credentials.email}
                        onChange={(e) => {
                            setCredentials({
                                ...credentials,
                                email: e?.target.value,
                            });
                        }}
                    ></input>
                </div>
                <div className="form-group">
                    <label className="label" htmlFor="password">
                        Password
                    </label>
                    <input
                        className="input"
                        id="password"
                        type="password"
                        placeholder="johndoe@example.com"
                        value={credentials.password}
                        onChange={(e) => {
                            setCredentials({
                                ...credentials,
                                password: e?.target.value,
                            });
                        }}
                    ></input>
                </div>

                {auth.errorMessage && (
                    <div className="form-group auth__error-message">
                        {auth.errorMessage}
                    </div>
                )}
                <button
                    className={"btn btn--primary auth__button"}
                    disabled={auth.statusMessage !== null ? true : false}
                >
                    {auth.statusMessage !== null
                        ? auth.statusMessage
                        : "Sign in"}
                </button>
            </form>
        </div>
    );
}
