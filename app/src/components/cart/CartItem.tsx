import "@/assets/css/components/cart/cart-item.css";
import { useEffect, useState } from "react";
import { MdClose } from "react-icons/md";

type CartItemProps = {
    product: CartProduct;
    handleUpdateProduct: (product: CartProduct) => void;
    handleDeleteProduct: (product: CartProduct) => void;
    setCalculating: (value: boolean) => void;
};

const CartItem: React.FunctionComponent<CartItemProps> = (props) => {
    const [quantityChanged, setQuantityChanged] = useState(false);
    const [quantity, setQuantity] = useState(props.product.quantity);

    const handleChangeInput = (e: React.FormEvent<HTMLInputElement>) => {
        props.setCalculating(true);
        setQuantity(+e.currentTarget.value);
        setQuantityChanged(true);
    };

    const handleClickRemoveProduct = () => {
        props.handleDeleteProduct(props.product);
    };

    useEffect(() => {
        const inter = setTimeout(() => {
            if (!quantityChanged) {
                return;
            }

            setQuantity(quantity);
            const productUpdated = { ...props.product, quantity: quantity };
            props.handleUpdateProduct(productUpdated);
        }, 1000);

        return () => {
            clearTimeout(inter);
        };
    }, [quantity]);

    return (
        <div className="cart-item">
            <div className="cart-item__description">
                <div className="cart-item__image-container">
                    <img
                        src={props.product.image}
                        alt={props.product.title}
                        className="cart-item__image"
                    />
                </div>
                <div className="cart-item__content">
                    <div className="cart-item__title">
                        {props.product.title}
                    </div>
                    {/* <div className="cart-item__size">
                        <span className="cart-item__label">Size:</span>
                        8.5
                    </div>
                    <div className="cart-item__color">
                        <span className="cart-item__label">Color:</span>
                        Blue
                    </div> */}
                    <div className="cart-item__price">
                        ${props.product.price}
                    </div>
                </div>
            </div>

            <div className="cart-item__actions">
                <div className="cart-item__quantity">Quantity</div>
                <div>
                    <input
                        type="number"
                        className="cart-item__quantity-input"
                        value={quantity}
                        onChange={handleChangeInput}
                    />
                </div>
                <button
                    className="cart-item__remove-btn"
                    onClick={handleClickRemoveProduct}
                >
                    <i className="cart-item__remove-btn-icon">
                        <MdClose />
                    </i>
                    remove
                </button>
            </div>
        </div>
    );
};

export default CartItem;
