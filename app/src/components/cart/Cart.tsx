"use client";

import "@/assets/css/components/cart/cart.css";
import { useAppDispatch, useAppSelector } from "@/hooks/reduxHooks";
import { update, deleteProduct, calculateSubtotal } from "@/store/cartSlice";
import TitleSection from "@/components/common/TitleSection";
import CartItem from "./CartItem";
import CartSubTotal from "./CartSubTotal";
import { useEffect, useState } from "react";
import { CSSTransition, TransitionGroup } from "react-transition-group";

export default function Checkout() {
    const cart = useAppSelector((state) => state.cartState);
    const dispatch = useAppDispatch();
    const [calculating, setCalculating] = useState(false);
    const [isMounted, setIsMounted] = useState(false);

    useEffect(() => setIsMounted(true), []);

    const handleUpdateProduct = (product: CartProduct) => {
        dispatch(update(product));
        setCalculating(false);
    };

    const handleDeleteProduct = (product: CartProduct) => {
        dispatch(deleteProduct(product));
    };

    useEffect(() => {
        dispatch(calculateSubtotal());
    }, []);

    const cartProducts = cart.products.map((product) => {
        return (
            <CSSTransition key={product.id} classNames="fade" timeout={500}>
                <CartItem
                    product={product}
                    handleUpdateProduct={handleUpdateProduct}
                    handleDeleteProduct={handleDeleteProduct}
                    setCalculating={setCalculating}
                />
            </CSSTransition>
        );
    });

    return (
        <section className="cart-section container mx-auto">
            <TitleSection title="Your Cart" />
            {isMounted && (
                <div className="grid lg:grid-cols-4 lg:gap-2">
                    <div className="lg:col-start-1 lg:col-span-3">
                        <TransitionGroup>{cartProducts}</TransitionGroup>
                    </div>
                    <div className="px-8">
                        <CartSubTotal
                            calculating={calculating}
                            subtotal={cart.subtotal}
                        />
                    </div>
                </div>
            )}
        </section>
    );
}
