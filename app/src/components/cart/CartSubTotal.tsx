import "@/assets/css/components/cart/cart-subtotal.css";
import Link from "next/link";
//import { useRef, useState } from "react";
//import { CSSTransition } from "react-transition-group";

type CartSubTotalProps = {
    calculating: boolean;
    subtotal: number;
};

export default function CartSubTotal(props: CartSubTotalProps) {
    let content = <p>Calculating ...</p>;
    /* const [toggle, setToggle] = useState(false);
    const nodeRef = useRef(null); */

    if (!props.calculating) {
        content = (
            <>
                <h2 className="cart-subtotal__title">Subtotal</h2>
                <h3 className="cart-subtotal__price">${props.subtotal}</h3>
                {/* <button
                    onClick={() => {
                        setToggle(!toggle);
                    }}
                >
                    Toggle
                </button>
                <CSSTransition
                    nodeRef={nodeRef}
                    in={toggle}
                    timeout={3000}
                    classNames="my-node"
                    unmountOnExit
                >
                    <div className="my-node" ref={nodeRef}>
                        {"I'll receive my-node-* classes"}
                    </div>
                </CSSTransition> */}

                {props.subtotal > 0 && (
                    <Link href="/cart" className="btn btn--primary">
                        Proceed to checkout
                    </Link>
                )}
            </>
        );
    }

    return <div className="cart-subtotal">{content}</div>;
}
