import { default as axiosPrivate } from "@/services/apiv3";
import { useEffect } from "react";
import useRefreshToken from "./useRefreshToken";

const useAxiosPrivate = () => {
    const refresh = useRefreshToken();

    useEffect(() => {
        console.log("entra al efecto useAxiosPrivate");

        const responseIntercept = axiosPrivate.interceptors.response.use(
            (response) => response,
            async (error) => {
                const prevRequest = error?.config;
                if (error?.response?.status === 401 && !prevRequest?.sent) {
                    console.log("Trriger 401 dentro de axios private");
                    prevRequest.sent = true;

                    await refresh();

                    return axiosPrivate(prevRequest);
                }
                return Promise.reject(error);
            }
        );

        return () => {
            console.log("se desmonta el efecto useAxiosPrivate");
            axiosPrivate.interceptors.response.eject(responseIntercept);
        };
    }, [refresh]);

    return axiosPrivate;
};

export default useAxiosPrivate;
