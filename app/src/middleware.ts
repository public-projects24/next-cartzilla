import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";

// This function can be marked `async` if using `await` inside
export function middleware(request: NextRequest) {
    if (request.nextUrl.pathname.startsWith("/profile")) {
        console.log("-------------------");
        console.log("middleware auth");
        console.log("-------------------");

        const jwt = request.cookies.get("jwt")?.value;
        const refreshJwt = request.cookies.get("refresh-jwt")?.value;

        console.log(
            "En servidor jwt es ",
            jwt,
            " y refreshJwt es ",
            refreshJwt
        );

        if (
            !request.cookies.has("jwt") &&
            !request.cookies.has("refresh-jwt")
        ) {
            console.log("no existe jwt ni refresh-jwt, redirigir al login");

            // Add requested page to url
            const requestedPage = request.nextUrl.pathname;
            const url = request.nextUrl.clone();
            url.pathname = `/login`;
            url.search = `requested=${requestedPage}`;

            return NextResponse.redirect(url);
        }

        return NextResponse.next();
    }

    if (request.nextUrl.pathname.startsWith("/login")) {
        console.log("-------------------");
        console.log("middleware guest");
        console.log("-------------------");

        const jwt = request.cookies.get("jwt")?.value;
        const refreshJwt = request.cookies.get("refresh-jwt")?.value;

        console.log(
            "En servidor jwt es ",
            jwt,
            " y refreshJwt es ",
            refreshJwt
        );

        if (request.cookies.has("jwt") || request.cookies.has("refresh-jwt")) {
            console.log("jwt o refresh-jwt existe, redirigir al home");
            return NextResponse.redirect(new URL("/", request.url));
        }

        return NextResponse.next();
    }

    //return NextResponse.redirect(new URL("/home", request.url));
}

// See "Matching Paths" below to learn more
/* export const config = {
    matcher: "/profile",
};
 */
