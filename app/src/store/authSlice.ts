import auth from "@/services/auth";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import Cookies from "js-cookie";
//import pru from "./pru";

export interface AuthState {
    isAuthenticated: boolean;
    user: { name: string; email: string } | null;
    statusMessage: string | null;
    errorMessage: string | null;
}

const initialState: AuthState = {
    isAuthenticated: false,
    user: null,
    statusMessage: null,
    errorMessage: null,
};

export const loginRequest = createAsyncThunk(
    "auth/loginRequest",
    async (
        credentials: { email: string; password: string },
        { rejectWithValue }
    ) => {
        try {
            const response = await auth.login(credentials);
            return response;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const logoutRequest = createAsyncThunk(
    "auth/logoutRequest",
    async (data, { rejectWithValue }) => {
        try {
            //console.log("data ", data);
            const response = await auth.logout();
            return response;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        clearErrorMessage: (state) => {
            state.errorMessage = null;
        },
    },
    /*  extraReducers: (builder) => {
        pru(builder);
    }, */
    extraReducers: (builder) => {
        builder.addCase(loginRequest.fulfilled, (state, action) => {
            console.log("fullfiled");
            console.log("state ", state);
            console.log("action ", action);
            console.log(action.payload);

            state.statusMessage = null;
            state.errorMessage = null;

            state.isAuthenticated = true;
            state.user = {
                name: action.payload.data.user.name,
                email: action.payload.data.user.email,
            };
        });
        builder.addCase(loginRequest.pending, (state) => {
            console.log("pending");
            state.statusMessage = "Sign in ...";

            /* console.log("state ", state);
            console.log("action ", action); */
        });
        builder.addCase(loginRequest.rejected, (state, action) => {
            console.log("rejected");
            /*  console.log("state ", state);
            console.log("action ", action); 
            console.log((action.payload as any).response);*/

            state.statusMessage = null;
            state.errorMessage = (action.payload as any).response.data;
        });
        builder.addCase(logoutRequest.fulfilled, (state, action) => {
            console.log("fullfiled lougout");
            console.log("state ", state);
            console.log("action ", action);
            console.log(action.payload);

            state.statusMessage = null;
            state.errorMessage = null;

            state.isAuthenticated = false;
            state.user = null;
        });
        builder.addCase(logoutRequest.pending, (state) => {
            console.log("pending logout");
            state.statusMessage = "Log out ...";
        });
        builder.addCase(logoutRequest.rejected, (state, action) => {
            console.log("rejected logout");
            console.log("state ", state);
            console.log("action ", action);
            // console.log((action.payload as any).response);

            /* state.statusMessage = null;
            state.errorMessage = (action.payload as any).response.data; */
        });
    },
});

// Action creators are generated for each case reducer function
export const { clearErrorMessage } = authSlice.actions;

export default authSlice.reducer;
