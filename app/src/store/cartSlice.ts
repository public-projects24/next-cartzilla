import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import Cookies from "js-cookie";

export interface CartState {
    products: CartProduct[];
    subtotal: number;
}

const initialState: CartState = {
    products: [],
    subtotal: 0,
};

const updateCookie = (products: CartProduct[]) => {
    Cookies.set("cart", JSON.stringify(products));
};

export const cartSlice = createSlice({
    name: "cart",
    initialState,
    reducers: {
        add: (state, action: PayloadAction<Product>) => {
            const cartProduct = {
                ...action.payload,
                quantity: 1,
            };
            const productFound = state.products.find(
                (product) => product.id === action.payload.id
            );

            if (productFound) {
                productFound.quantity++;
                return;
            }

            state.products.push(cartProduct);
        },
        update: (state, action: PayloadAction<CartProduct>) => {
            const productFound = state.products.find(
                (product) => product.id === action.payload.id
            );

            if (productFound) {
                productFound.quantity = action.payload.quantity;
                cartSlice.caseReducers.calculateSubtotal(state);
            }
        },
        deleteProduct: (state, action: PayloadAction<CartProduct>) => {
            const newProducts = state.products.filter((product) => {
                if (product.id !== action.payload.id) {
                    return product;
                }
            });
            state.products = newProducts;

            cartSlice.caseReducers.calculateSubtotal(state);
        },
        calculateSubtotal: (state) => {
            state.subtotal = 0;
            state.products.forEach((product) => {
                state.subtotal += Math.round(product.price * product.quantity);
            });
        },
    },
});

// Action creators are generated for each case reducer function
export const { add, update, deleteProduct, calculateSubtotal } =
    cartSlice.actions;

export default cartSlice.reducer;
