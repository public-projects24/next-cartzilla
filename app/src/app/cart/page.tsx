import Cart from "@/components/cart/Cart";

export default function CartPage() {
    return (
        <section>
            <Cart></Cart>
        </section>
    );
}
