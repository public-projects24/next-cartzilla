import { Suspense } from "react";
import Banner from "@/components/common/Banner";
import LatestProducts from "@/components/home/LatestProducts";
import TrendingProducts from "@/components/home/TrendingProducts";

export default async function Home() {
    return (
        <div>
            <Banner />

            <Suspense fallback={<div>Loading trending products ...</div>}>
                <TrendingProducts />
            </Suspense>

            <Suspense fallback={<div>Loading latest products ...</div>}>
                <LatestProducts />
            </Suspense>
        </div>
    );
}
