import type { Metadata } from "next";
import ReduxProvider from "@/provider/redux/ReduxProvider";

import "@/assets/css/app.css";
import NavMenu from "@/components/common/NavMenu";
import { jwtCookieExistAction } from "@/app/actions";

export const metadata: Metadata = {
    title: "Cartzilla",
    description: "A Cartzilla using next",
};

export default function RootLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return (
        <ReduxProvider>
            <html lang="en">
                <body>
                    <NavMenu jwtCookieExistAction={jwtCookieExistAction} />
                    {children}
                </body>
            </html>
        </ReduxProvider>
    );
}
