"use server";

import { revalidatePath } from "next/cache";
import { cookies } from "next/headers";
import { redirect } from "next/navigation";

export async function jwtCookieExistAction() {
    console.log("-------------------");
    console.log("jwtCookieExistAction");
    console.log("-------------------");
    const cookiesList = cookies();
    const hasJwtCookie = cookiesList.has("jwt");
    const hasRefreshCookie = cookiesList.has("refresh-jwt");
    console.log(
        "hasCookie jwt ",
        hasJwtCookie,
        "hasRefreshCookie ",
        hasRefreshCookie
    );

    /*  if (hasJwtCookie || hasRefreshCookie) {
        console.log("alguno existe");
        return true;
    } else {
        console.log("no existe ninguno");
    }
    return false; */
    return hasJwtCookie || hasRefreshCookie ? true : false;
}

export async function revalidateUrlAction(path: string) {
    console.log("-------------------");
    console.log("revalidateUrlAction");
    console.log("-------------------");

    console.log("revalidated ", path);
    revalidatePath(path);
    console.log("redirigir en action a: ", path);
    redirect(path);
}
