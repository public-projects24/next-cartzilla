import Login from "@/components/auth/Login";
import { jwtCookieExistAction, revalidateUrlAction } from "@/app/actions";

export default function LoginPage() {
    return (
        <Login
            jwtCookieExistAction={jwtCookieExistAction}
            revalidateUrlAction={revalidateUrlAction}
        ></Login>
    );
}
