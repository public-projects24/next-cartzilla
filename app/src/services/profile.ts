import api from "@/services/apiv2";

export default {
    get() {
        return api.get(`profile`);
    },
};
