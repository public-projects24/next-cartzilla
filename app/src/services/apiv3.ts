import axios from "axios";

const api = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_AUTH_ENDPOINT,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
    },
    withCredentials: true,
});

/* api.interceptors.response.use(
    function (response) {
        // Any status code that lie within the range of 2xx cause this function to trigger
        // Do something with response data
        return response;
    },
    function (error) {
        // Any status codes that falls outside the range of 2xx cause this function to trigger
        // Do something with response error
        interceptorOnError(error);
        return Promise.reject(error);
    }
);

async function interceptorOnError(error: any) {
    console.log("error interceptor ", error.response);
    //console.log("the route is: ", router.currentRoute);

    const prevRequest = error?.config;
    console.log("prevRequest ", prevRequest);
    // If user is unauthenticated
    if (error.response.status === 401) {
        console.log("error status 401");
        if (!prevRequest?.sent) {
            console.log("prevRequest.sent no existe ", prevRequest?.sent);
            prevRequest.sent = true;

            const refreshResponse = await api.post(`refresh-token`, {});
            console.log("refreshResponse ", refreshResponse);

            //return api(prevRequest);
        }
        // if (router.currentRoute.name != "login") {
        //     console.log("the route is different from login");
        //     await store.dispatch("auth/logout");
        //     router.push({ name: "login" });
        // }
    }
} */
export default api;
