import api from "./api";

export default {
    /* paginate(offset: number, limit: number) {
        return api.get(`products?offset=${offset}&limit=${limit}`);
    }, */
    limit(limit: number) {
        return api.get(`products?limit=${limit}`);
    },

    /*  update(data) {
        data["_method"] = "put";
        return Api.post(
            "/api/canvas",
            data,
            store.getters["auth/getAuthorizationHeaders"]
        );
    }, */
};
