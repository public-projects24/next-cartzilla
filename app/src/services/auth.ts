import api from "@/services/apiv2";

export default {
    login(credentials: object) {
        return api.post(`login`, credentials);
    },
    logout() {
        return api.post(`logout`);
    },
};
